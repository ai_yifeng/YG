&emsp; 

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

### &#x1F449; [点击进入 FAQs](https://gitee.com/arlionn/YG/wikis/YG-Day1-WangCT.md?sort_id=3098277) 


&emsp;

本次课程共安排了 13 位经验丰富的助教，与大家共同交流、进步。大家提问时，可以在群里 `@助教` 以便及时获得回应。

### 提问须知
- **温馨提示：** 由于精力有限，与课程内容无关的问题仅在助教们力所能及的情况下给与回复，望见谅。
- 大家可以在课程微信群中就课程中涉及的问题提问，提问前可以先到 [连享会问答区](https://gitee.com/arlionn/WD) 和 [连享会暑期班 FAQs](https://gitee.com/arlionn/PX/wikis/%E8%BF%9E%E7%8E%89%E5%90%9B-Stata%E5%AD%A6%E4%B9%A0%E5%92%8C%E5%90%AC%E8%AF%BE%E5%BB%BA%E8%AE%AE.md?sort_id=2662737) 查看是否已经有相似的问题。亦可到 [连享会推文集锦](https://www.lianxh.cn/news/d4d5cd7220bc7.html) 页面，输入 `Ctrl+F` 快捷键搜索关键词，查看相应推文。搜索问题的过程，也是有效学习的过程。 
- 若上述途径都无法找到合适的解答，请详述你的问题背景，提出问题。比如，你要介绍是第几讲第几页或第几行讲义出现的问题，你的疑虑或错误信息是什么 (可以配合截图，微信截图快捷键为 `Alt+A`)。

### 答疑时间安排

- 课前：8:00-8:50 助教答疑时段
- 课后 1：下午 17:30-18:00 为主讲老师答疑时段
- 课后 2：下午 19:30-22:00 为助教答疑时段
- 每天的答疑汇总文档会发布于 [因果推断-FAQs](https://gitee.com/arlionn/YG/wikis) 页面

### 助教小组分工 

- **Day1**：张德亮 (组长)   王洪鹏 杨继超 杜思佳
- **Day2**：徐云娇 (组长)   秦利宾 甘徐沁
- **Day3**：涂漫漫 (组长)   刘雅玄 杨雅程
- **Day4**：孙碧洋 (组长)   苗妙 孙爱晶


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

> [听课建议](https://www.lianxh.cn/news/69706e871c9ad.html) || [Stata资源链接](https://www.lianxh.cn/news/46917f1076104.html) || [Stata Journal](https://www.lianxh.cn/news/12ffe67d8d8fb.html) || [推文分类](https://www.lianxh.cn/news/d4d5cd7220bc7.html)